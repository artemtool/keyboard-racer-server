import * as express from 'express';
import { verifyJWT } from './../middlewares';
import { carManager } from './../managers/index';
import { PageableListParameters } from './../../../shared/interfaces/pageableListParameters';
import { IPage } from '../../../shared/interfaces/page';
import { ICar } from './../../../shared/interfaces/car';
import { CarModel } from '../../../shared/enums/carModel';

const router = express.Router();

router.post('/list/onsale', verifyJWT, async (req, res) => {
  const userId: string = req['decodedToken'].payload.userId;
  const parameters: PageableListParameters = req.body;

  const carsOnSale: IPage<ICar> = await carManager.getCarsOnSale(userId, parameters);
  res.send(carsOnSale);
});

router.post('/buy/:model', verifyJWT, async (req, res) => {
  const userId: string = req['decodedToken'].payload.userId;
  const model: CarModel = Number(req.params.model);

  try {
    const car: ICar = await carManager.buyCar(userId, model);
    res.send(car);
  } catch (error) {
    res
      .status(400)
      .send(error.message);
  }
});

export default router;