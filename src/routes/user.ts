import * as express from 'express';
import { verifyJWT } from './../middlewares';
import { userManager } from '../managers';
import { PageableListParameters } from './../../../shared/interfaces/pageableListParameters';
import { IUser } from './../../../shared/interfaces/user';
import { IPage } from '../../../shared/interfaces/page';

const router = express.Router();

router.post('/signup', async (req, res) => {
  const {
    email,
    password,
    nickname
  } = req.body;
  await userManager.createUser(email, password, nickname);

  res.sendStatus(200);
});

router.post('/signin', async (req, res) => {
  const {
    email,
    password
  } = req.body;

  const user = await userManager.getUserByEmail(email);

  if (user && userManager.checkUserPassword(user, password)) {
    const accessToken = userManager.createAccessToken(user);
    user.password = null;

    res.send({
      user,
      accessToken
    });
  } else {
    res.sendStatus(400);
  }
});

router.get('/iam', verifyJWT, async (req, res) => {
  const id: string = req['decodedToken'].payload.userId;
  const user = await userManager.getUserById(id);

  res.send(user);
});

router.get('/signout', verifyJWT, (req, res) => {
  res.send(200);
});

router.post('/rating', verifyJWT, async (req, res) => {
  const parameters: PageableListParameters = req.body;
  const page: IPage<IUser> = await userManager.getGlobalRating(parameters);

  res.send(page);
});

export default router;