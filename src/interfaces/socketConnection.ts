export interface SocketConnection {
  socketId: string;
  userId: string;
}