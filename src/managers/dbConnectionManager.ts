import { MongoClient, MongoError, Db } from "mongodb";

export class DBConnectionManager {
  private readonly DB_NAME: string;
  private readonly DB_CONNECTION_STRING: string;

  constructor(dbName: string, dbConnectionString: string) {
    this.DB_NAME = dbName;
    this.DB_CONNECTION_STRING = dbConnectionString;
  }

  public connect(): Promise<Db> {
    return new Promise((resolve, reject) => {
      MongoClient.connect(
        this.DB_CONNECTION_STRING,
        {
          useNewUrlParser: true
        },
        (error: MongoError, client: MongoClient) => {
          if (error) {
            reject(error);
          } else {
            const db = client.db(this.DB_NAME);
            resolve(db);
          }
        }
      );
    });
  }
}