import { CollectionManager } from './collectionManager';
import { IRace } from '../../../shared/interfaces/race';
import { RaceStatus } from '../../../shared/enums/raceStatus';
import { CarModel } from '../../../shared/enums/carModel';
import { ObjectId } from 'mongodb';
import { PageableListParameters } from './../../../shared/interfaces/pageableListParameters';
import { IPage } from './../../../shared/interfaces/page';

export class RaceManager {
  constructor(
    private readonly collectionManager: CollectionManager
  ) { }

  public getRaceById(id: string): Promise<IRace> {
    return this.collectionManager.races
      .findOne({ _id: new ObjectId(id) });
  }

  public async createRace(creatorId: string, membersNeeded: number): Promise<IRace> {
    const articlesTotalAmount = await this.collectionManager.articles.count();
    const skipAmount = Math.floor(Math.random() * articlesTotalAmount);
    const articles = await this.collectionManager.articles
      .find()
      .limit(1)
      .skip(skipAmount)
      .toArray();

    const race: IRace = {
      _id: null,
      winnerId: null,
      creatorId,
      date: new Date().toISOString(),
      members: [],
      membersNeeded,
      status: RaceStatus.MembersWaiting,
      article: articles[0]
    };

    return this.collectionManager.races
      .insertOne(race)
      .then(result => {
        race._id = result.insertedId.toHexString();
        return race;
      });
  }

  public async joinRace(
    raceId: string,
    userId: string,
    car: CarModel
  ): Promise<IRace> {
    const race = await this.collectionManager.races.findOne({ _id: new ObjectId(raceId) });
    const user = await this.collectionManager.users.findOne({ _id: new ObjectId(userId) });

    if (race) {
      const isCheckInOpened = race.members.length < race.membersNeeded;
      const alreadyJoinedIndex = race.members.findIndex(m => m.userId === userId);

      if (alreadyJoinedIndex === -1) {
        if (isCheckInOpened) {
          race.members.unshift({
            userId: userId,
            userNickname: user.nickname,
            typedLength: 0,
            CPM: 0,
            car,
            place: null
          });

          return this.collectionManager.races
            .save(race)
            .then(() => race);
        } else {
          return Promise.resolve(null);
        }
      } else {
        const raceMember = race.members.splice(alreadyJoinedIndex, 1);
        race.members.unshift(raceMember[0]);
        return race;
      }
    } else {
      return Promise.resolve(null);
    }
  }

  public async updateRace(raceId: string, race: IRace): Promise<void> {
    await this.collectionManager.races.save(race);
  }

  public async getUserRaces(userId: string, parameters: PageableListParameters): Promise<IPage<IRace>> {
    const cursor = await this.collectionManager.races
      .find({
        members: {
          '$elemMatch': {
            '$and': [
              { userId },
              {
                place: {
                  '$ne': null
                }
              }
            ]
          }
        }
      });
    const total = await cursor.count();
    const entities = await cursor
      .sort(parameters.orderBy, parameters.isDesc ? -1 : 1)
      .skip((parameters.page - 1) * parameters.perPageAmount)
      .limit(parameters.perPageAmount)
      .toArray();

    return {
      entities,
      total
    };
  }
}