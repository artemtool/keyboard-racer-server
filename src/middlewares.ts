import { verifyJWTToken } from './common/auth'
import { Request, Response, NextFunction } from 'express';
import { Socket } from 'socket.io';

export function verifyJWT(req: Request, res: Response, next: NextFunction) {
  const authHeader = req.headers['authorization'];
  let jwtToken;
  if (authHeader) {
    const regExp: RegExp = /Bearer (.+)/;
    jwtToken = regExp.exec(authHeader)[1];

    verifyJWTToken(jwtToken)
      .then(decodedToken => {
        req['decodedToken'] = decodedToken;
        next();
      })
      .catch((err) => {
        res.status(403)
          .send({ message: "Authentication error" })
      });
  } else {
    res.status(403)
      .send({ message: "Authentication error" });
  }
}

export function verifyJWTOnSocketConnection(socket: Socket, next: NextFunction) {
  const jwtToken = socket.handshake.query && socket.handshake.query.accessToken;
  if (jwtToken) {
    verifyJWTToken(jwtToken)
      .then(decodedToken => {
        socket['decodedToken'] = decodedToken;
        next();
      })
      .catch((err) => {
        next(new Error('Authentication error'));
      });
  } else {
    next(new Error('Authentication error'));
  }
}

export function handleCors(req: Request, res: Response, next: NextFunction) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
}

export function handleErrors(req: Request, res: Response, next: NextFunction) {
  try {
    next();
  } catch (err) {
    console.error(err);

    res.status(500)
      .send({ message: 'Internal server error' });
  }
}